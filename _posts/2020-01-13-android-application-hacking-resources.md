---
layout: post
title:  "Android Application Hacking Resources"
date:   2020-01-13
author: dee-see
last_modified_at: 2022-10-16
redirect_from:
  - /android/security/2020/01/13/android-resources.html
categories: android security
---

These are links that I found interesting as I was (and still am) learning about Android application security and I'm putting it here in case it can help someone else!

Last update: {{ page.last_modified_at }}

## Aggregators, news feeds, twitter threads, etc.

Come back to those every now and then to see if they have new content!

- ["android" HackerOne Hacktivity](https://hackerone.com/hacktivity?querystring=android&filter=type:public&order_direction=DESC&order_field=latest_disclosable_activity_at)
- [Twitter thread with tons of great links](https://twitter.com/fs0c131y/status/1129680329994907648)
- [Android-Reports-and-Resources GitHub repository](https://github.com/B3nac/Android-Reports-and-Resources)
- [awesome-mobile-security GitHub repository](https://github.com/vaib25vicky/awesome-mobile-security)
- [Android_Security repository](https://github.com/anantshri/Android_Security)
- [#AndroidHackingMonth](https://twitter.com/hashtag/AndroidHackingMonth) (HackerOne's Android Hacking Month in February 2020)
- [AllThingsAndroid GitHub repository](https://github.com/jdonsec/AllThingsAndroid)
- [Android Partner Vulnerability Initiative bug tracker](https://bugs.chromium.org/p/apvi/issues/list?q=&can=1) (details of bugs specific to the Android OEM code)
- Oversecured [blog](https://blog.oversecured.com/) and [HackerOne profile](https://hackerone.com/oversecured)

## Videos

- [jiska - Finding and Backtracing Signal Messages on Android](https://www.youtube.com/watch?v=oy0mn5CV-ro) (Shows how to use `frida-trace`)
- [Maddie Stone - Securing the System: A Deep Dive into Reversing Android Pre-Installed Apps](https://www.youtube.com/watch?v=U6qTcpCfuFc)
- [Baptiste Robert aka fs0c131ty - L'histoire de la découverte d'une backdoor signée OnePlus](https://www.youtube.com/watch?v=XyczLWRnD8M) (It's in French)
- [Ben Actis - Advanced Android Bug Bounty skills](https://www.youtube.com/watch?v=OLgmPxTHLuY)
- [Yekaterina Tsipenyul O'Neil & Erika Chin - Seven Ways to Hang Yourself with Google Android](https://www.youtube.com/watch?v=-1xAr_tHMKA) (From 2011 but still interesting to this day)
- [Dawn Isabel - Fun with Frida on Mobile](https://www.youtube.com/watch?v=dqA38-1UMxI) (It's for iOS but the same ideas can be used on Android)
- [Sebastian Porst & Google Play - Overview of common Android app vulnerabilities](https://www.youtube.com/watch?v=51S8PeuzlmI)
- [Nikita Stupin - Vulnerabilities of mobile OAuth 2.0](https://www.youtube.com/watch?v=vjCF_O6aZIg)
- [B3nac - Android Hacking](https://www.youtube.com/watch?v=mr64si_-YwI)
- [B3nac - Android Application Exploitation](https://www.youtube.com/watch?v=AqVMfZAboCg)
- [B3nac - Exploiting Android deep links and exported components](https://www.youtube.com/watch?v=lg1sN8njSYs)
- Maddie Stone - Android App Reverse Engineering Workshop ([part 1](https://www.youtube.com/watch?v=BijZmutY0CQ), [part 2](https://www.youtube.com/watch?v=xBk_2_JiCSg))

## Write-ups, guides and blog articles

- [Proxying Android app traffic – Common issues / checklist](https://blog.nviso.eu/2020/11/19/proxying-android-app-traffic-common-issues-checklist/)
- [Bug Bounty Hunting Tips #2 —Target their mobile apps (Android Edition)](https://medium.com/bugbountyhunting/bug-bounty-hunting-tips-2-target-their-mobile-apps-android-edition-f88a9f383fcc)
- [Quarkslab's "diffing" blog posts](https://blog.quarkslab.com/tag/diffing.html) (They have an unreleased android diffing engine, we can't use it but the next best thing is reading about it!)
- [Configuring Burp Suite With Android Nougat](https://blog.ropnop.com/configuring-burp-suite-with-android-nougat/)
- [Universal XSS in Android WebView (CVE-2020-6506)](https://alesandroortiz.com/articles/uxss-android-webview-cve-2020-6506/)
- [Samsung Find My Mobile vulnerability](http://char49.com/tech-reports/fmmx1-report.pdf)
- [OWASP Mobile Security Testing Guide](https://mobile-security.gitbook.io/mobile-security-testing-guide/)
- r2-pay challenge write-up ([part 1 (anti-debug, anti-root & anti-frida)](https://www.romainthomas.fr/post/20-09-r2con-obfuscated-whitebox-part1/), [part 2 (whitebox)](https://www.romainthomas.fr/post/20-09-r2con-obfuscated-whitebox-part2/))
- [Security of mobile OAuth 2.0](https://habr.com/ru/company/mailru/blog/456702/)
- [Proxying Android app traffic – Common issues / checklist](https://blog.nviso.eu/2020/11/19/proxying-android-app-traffic-common-issues-checklist/)

## Social media accounts

### Telegram

- [fs0c131y](https://t.me/s/fs0c131yOfficialChannel)
- [Android Security & Malware](https://t.me/s/androidMalware)

### Twitter

- [maddiestone](https://twitter.com/maddiestone)
- [mobilesecurity_](https://twitter.com/mobilesecurity_)
- [fs0c131y](https://twitter.com/fs0c131y)
- [reyammer](https://twitter.com/reyammer)
- [LibraAnalysis](https://twitter.com/LibraAnalysis)
- [B3nac](https://twitter.com/B3nac)
- [maldr0id](https://twitter.com/maldr0id)
- [\_bagipro](https://twitter.com/_bagipro)

## Courses

- [MOBISEC](https://mobisec.reyammer.io/)
- [Android App Reverse Engineering 101 by Maddie Stone](https://maddiestone.github.io/AndroidAppRE/)

## Tools

- [AERoot](https://github.com/quarkslab/AERoot) - AERoot is a command line tool that allows you to give the root privileges on-the-fly to any process running on the Android emulator with Google Play flavors AVDs
- [Mobile Security Framework (MobSF)](https://github.com/MobSF/Mobile-Security-Framework-MobSF)
- [Frida](https://frida.re/docs/android/)
  - [Bypass Certificate Pinning](https://blog.jamie.holdings/2019/01/19/advanced-certificate-bypassing-in-android-with-frida/)
  - [Frida Cheatsheet and Code Snippets for Android](https://erev0s.com/blog/frida-code-snippets-for-android/)
  - [Intercepting traffic from Android Flutter applications](https://blog.nviso.eu/2019/08/13/intercepting-traffic-from-android-flutter-applications/)
- [Drozer](https://github.com/fsecurelabs/drozer/)
  - [Getting started guide](https://securitygrind.com/using-the-drozer-framework-for-android-pentesting/)
- [Jalangi2](https://github.com/Samsung/jalangi2)
- [KeyHacks](https://github.com/streaak/keyhacks) - Instructions to validate that leaked tokens are valid
- [NotKeyHacks](https://gitlab.com/dee-see/notkeyhacks) - List of tokens that aren't sensitive even though they might appear to be

