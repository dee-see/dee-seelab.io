#!/usr/bin/bash
bundle exec jekyll clean
JEKYLL_ENV=LOCAL bundle exec jekyll s --config _config.yml,_config.local.yml --incremental --future
